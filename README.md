
# Introduction


Mako / Revo works fine on the PSION REVO

Sarge debian works fine on the PSION 5. 



# Description

Psions come with an operating system called Epoch, which is tolerable but not very good at talking to other machines, and there aren't very many apps available for it compared to PalmOS.

Syncing to Epoch from Linux

The Linux howtos weren't much help, but I eventually did figure out how to get my Mako talking to Linux, and it was fairly easy (too bad they don't just tell you that straight out. :-)

Communicating with a Mako/Revo on Linux

Find and install plptools. Avoid version 0.10 -- it fails utterly for most people (ncpd dies if the Mako shuts off, often refuses to start, but running as root in debug mode crashed less often). Version 0.9 works fairly well.

````
mkdir /mnt/psion
Run ncpd.
Run plpnfsd
cd /mnt/psion and poke around -- the Mako's file systems are NFS mounted like magic!
````

Psion Revo Archives Software for the Revo/Mako. Not Linux specific. Sort of a "Palmgear" site for Epoch.
Some links on communication between Epoch and Linux
PLPtools home page
Linux and Psion HOWTO
PsiLin home page in French and English summary


![](media/screendump5MX_03.jpg)


# Techs


````
Series 5mx called the MC218.

Processor - 32-bit ARM 710T CPU (RISC based), running at 36.864 MHz

Internal Memory - 16, 24, or 32MB RAM

Internal Memory - 10 MB ROM

Removable Disk Type - Type I Compactflash (CF) Disks

   (disks of up to 1+ GB MB have been shown to work)

Display Resolution - 640x240 (Half-VGA)

Display Type - Monochrome touch-screen (16 shades)

Default OS - EPOC (32 bit, multitasking)

Serial Ports - Standard RS232 and SIR Infrared; up to 230400 baud. 

Power - 2 AA batteries, backed up by a CR2032 lithium battery.

   Approx. 30 hrs of operation.  Optional 6V external power.

Screen Backlight

Sound - 1/2 W, 8 ohm loudspeaker  OKI MSM7717 and MSC1192 chipsets.

Microphone - electret with active gain control.

Keyboard - 53 key, QWERTY layout

Size - 172X89X24 mm

Weight - 350g (with 2 AA batteries)

Operating Temperature - 0 to 40 C
````

